-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: fitness
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','$2a$12$J5IiyQJNnSneD2k/Xhd0v.f9WwER3S9op5UpuIyje1OFneoYpDD5.'),(2,'Marina','$2a$10$XcAU6/5iJTBnphTqBN8FLuiFc6gCgDOG0qsxK.OWPMm4w.odni.ai');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment` (
  `appointment_id` int NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) NOT NULL,
  `client_phone` varchar(255) NOT NULL,
  `appointment_date` date NOT NULL,
  `trainer_name` varchar(255) NOT NULL,
  PRIMARY KEY (`appointment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (5,'Александр','+7(910)840-99-10','2024-06-14','Илья Леонтьев'),(6,'Мария','+7(980)855-31-12','2024-06-14','Аношенко Юлия');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `working_hours` varchar(255) NOT NULL,
  `vk_link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'+7 (800) 200-96-00','https://yandex.ru/maps/10839/rybinsk/?from=api-maps&ll=38.765790%2C58.057231&mode=routes&origin=jsapi_2_1_79&rtext=~58.057231%2C38.765790&rtt=auto&ruri=~ymapsbm1%3A%2F%2Fgeo%3Fdata%3DIgoNKxAbQhWbOmhC&z=16','Понедельник-пятница: 7:30 - 22:30  Суббота-воскресенье: 9:00 - 21:00','https://vk.com/vikonda.fitness');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `service_id` int NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) NOT NULL,
  `service_description` varchar(255) DEFAULT NULL,
  `link_exists` varchar(255) NOT NULL,
  `image_link` varchar(255) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Более 50 тренажеров','в многофункциональном зале','/services/gym','/images/services/trenajor.svg'),(2,'Зал групповых занятий','широкий выбор групповых программ','/services/yoga','/images/services/gymgroup.svg'),(3,'Фитнес бар','большой выбор спортивного питания','','/images/services/bar.svg'),(4,'Зал единоборств','для новичков, профессионалов','/services/martial-arts','/images/services/martial.svg'),(5,'Услуги тренера','только лучшие специалисты','/team','/images/services/trainer.svg'),(6,'Свободный доступ к инвентарю','(пояса, резинки)','','/images/services/inventary.svg'),(7,'Вода в зале и каждой раздевалке',NULL,'','/images/services/water.svg'),(8,'Детская комната Джунгли для держателей абонементов',NULL,'','/images/services/jungle.svg'),(9,'Просторные раздевалки мужская и женская',NULL,'','/images/services/cl.svg');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainer`
--

DROP TABLE IF EXISTS `trainer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trainer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `trainer_name` varchar(255) NOT NULL,
  `trainer_description` text NOT NULL,
  `trainer_phone` varchar(30) DEFAULT NULL,
  `trainer_vk_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainer`
--

LOCK TABLES `trainer` WRITE;
/*!40000 ALTER TABLE `trainer` DISABLE KEYS */;
INSERT INTO `trainer` VALUES (1,'Ястребова Кристина','Действующая спортсменка, занимается более 6 лет спортом. Имеет взрослый разряд по лёгкой атлетике. Специализация: спринт. Образование: ГПО АУ ЯО Рыбинский профессионально - педагогический колледж. Факультет: Физическая культура. Проходит дальнейшее обучение в ЯГПУ им. К.Д Ушинского. Тренер тренажерного зала.','+7 920 659-48-28','https://vk.com/fit.krist'),(2,'Соколов Никита','Закончил педагогический колледж, факультет «Физическая культура». В настоящее время получает высшее образование в ЯГПУ им. Ушинского на факультете «Физическая культура и спорт». В нашем фитнес клубе Никита работает персональным тренером с октября 2016 года. Специализация: силовые тренировки, фитнес.','+7 (980) 773-28-24','https://vk.com/gl_and_hf95'),(3,'Филиппов Максим','Образование: Магистратура ЯГПУ им.К.Д. Ушинского по специальности преподаватель «Физической культуры». Диплом школы фитнеса «Start Doing». Диплом федерации бодибилдинга и фитнеса Ярославской области о получении квалификации инструктор по фитнесу. Удостоверение о повышении квалификации по специальности «Директор фитнес центра». Опыт работы 11 лет. Основная специализация: Коррекция фигуры, набор мышечной массы, восстановление после травм. Со мной ты станешь: сильнее, выносливее, счастливее и худее. И самое главное — сохранишь и улучшишь состояние здоровья.','+7 (910) 826-70-05','https://vk.com/russian_forever'),(4,'Аношенко Юлия','Образование по специальности преподаватель \"Физической культуры\" (красный диплом). Студентка ЯГПУ им. К.Д. Ушинского. Стаж тренерской работы: 6 лет. Многократный призер областных соревнований по легкой атлетике. Сертифицированный фитнес-инструктор по специализации персональный тренинг. Сертифицированный фитнес-диетолог по направлению «Коррекция избыточного веса». Вместе с вами 24/7.','+7 (905) 647-99-48','https://vk.com/ssmolinovaa.coach'),(5,'Катерина Верховцева','Образование: Закончила Педагогический Колледж. Являюсь студенткой Смоленского Государственного Университета Спорта. Являюсь членом сборной команды России по гиревому спорту. Первый мастер спорта среди девушек в Ярославской области. Специализация: похудение, набор мышечной массы, поддержание физической формы.','+7 (999) 234-13-55','https://vk.com/katoshenka'),(6,'Илья Леонтьев','Опыт работы более 10 лет. Образование: Рыбинское медицинское училище (фельдшер). Колледж фитнеса и бодибилдинга им Бена Вейдера. 1-й разряд по пауэрлифтингу I.P.F. Специализация: пауэрлифтинг, бодибилдинг.','+7 (905) 639-80-43','');
/*!40000 ALTER TABLE `trainer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'fitness'
--

--
-- Dumping routines for database 'fitness'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-16 22:27:54
