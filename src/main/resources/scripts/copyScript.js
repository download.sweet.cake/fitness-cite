document.addEventListener('DOMContentLoaded', function() {
  const copyableItems = document.querySelectorAll('.copyable-text');
  copyableItems.forEach(item => {
    item.addEventListener('click', () => {
      const textToCopy = item.textContent;
      copyToClipboard(textToCopy);
    });
  });

  function copyToClipboard(text) {
    const dummyInput = document.createElement('input');
    dummyInput.style.opacity = 0;
    document.body.appendChild(dummyInput);
    dummyInput.setAttribute('value', text);
    dummyInput.select();
    document.execCommand('copy');
    document.body.removeChild(dummyInput);
  }
});
