document.addEventListener('DOMContentLoaded', () => {
    const carousel = document.querySelector('.carousel');
    const items = document.querySelectorAll('.carousel-item');
    const prevButton = document.querySelector('.carousel-prev');
    const nextButton = document.querySelector('.carousel-next');
    let currentIndex = 1;

    const firstClone = items[0].cloneNode(true);
    const lastClone = items[items.length - 1].cloneNode(true);

    firstClone.classList.add('clone');
    lastClone.classList.add('clone');

    carousel.appendChild(firstClone);
    carousel.insertBefore(lastClone, items[0]);

    const updateCarousel = (animate = true) => {
        const width = items[0].clientWidth;
        carousel.style.transition = animate ? 'transform 0.5s ease' : 'none';
        carousel.style.transform = `translateX(-${currentIndex * width}px)`;
    };

    prevButton.addEventListener('click', () => {
        if (currentIndex > 0) {
            currentIndex--;
            updateCarousel();
        }
        if (currentIndex === 0) {
            setTimeout(() => {
                carousel.style.transition = 'none';
                currentIndex = items.length;
                updateCarousel(false);
            }, 500);
        }
    });

    nextButton.addEventListener('click', () => {
        if (currentIndex < items.length + 1) {
            currentIndex++;
            updateCarousel();
        }
        if (currentIndex === items.length + 1) {
            setTimeout(() => {
                carousel.style.transition = 'none';
                currentIndex = 1;
                updateCarousel(false);
            }, 500);
        }
    });

    const preloadImages = () => {
        const imgElements = document.querySelectorAll('.carousel-item img');
        imgElements.forEach(img => {
            const src = img.getAttribute('src');
            const preloadedImg = new Image();
            preloadedImg.src = src;
        });
    };

    preloadImages();
    window.addEventListener('resize', () => updateCarousel(false));
    updateCarousel(false);
});
