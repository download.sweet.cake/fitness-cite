function init() {
  var myMap = new ymaps.Map("map", {
    center: [58.057231, 38.765790],
    zoom: 16,
    controls: ['zoomControl']
  });

  myMap.events.add('wheel', function (e) {
    e.preventDefault();
    if (e.get('delta') > 0) {
      myMap.setZoom(myMap.getZoom() + 1, {checkZoomRange: true});
    } else {
      myMap.setZoom(myMap.getZoom() - 1, {checkZoomRange: true});
    }
  });

  myMap.geoObjects.add(new ymaps.Placemark([58.057231, 38.765790], {
    balloonContent: 'г. Рыбинск, ул. Бабушкина, 29'
  }));

  myMap.behaviors.disable('scrollZoom');

  var mapElement = document.getElementById('map');

  function preventDefault(e) {
    e.preventDefault();
  }

  mapElement.addEventListener('mouseenter', function() {
    window.addEventListener('wheel', preventDefault, { passive: false });
  });

  mapElement.addEventListener('mouseleave', function() {
    window.removeEventListener('wheel', preventDefault, { passive: false });
  });
}

ymaps.ready(init);
