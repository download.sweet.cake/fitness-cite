package com.filippov.fitness.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ServiceModel {
    private Long id;
    @NotNull
    @Size(min=3, message="Некорректное наименование!")
    private String serviceName;
    private String serviceDescription;
    private String linkExists;
    private String imageLink;
}
