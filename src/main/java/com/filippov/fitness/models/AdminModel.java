package com.filippov.fitness.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AdminModel {

    private Long id;
    private String username;
    private String password;
}
