package com.filippov.fitness.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppointmentModel {
    private Integer id;

    @NotNull
    @Size(min=2, message="Некорректное имя!")
    private String clientName;

    @Pattern(regexp="^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", message="Некорректный телефон!")
    private String clientPhone;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date appointmentDate = new Date();

    @NotNull
    @Size(min=2, message="Некорректное имя тренера!")
    private String trainerName;
}
