package com.filippov.fitness.models;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrainerModel {
    private int id;
    @NotNull
    @Size(min=3, message="Некорректное имя!")
    private String trainerName;
    @NotNull
    @Size(min=3, message="Некорректное описание!")
    private String trainerDescription;
    private String trainerPhone;
    private String trainerVkLink;
}
