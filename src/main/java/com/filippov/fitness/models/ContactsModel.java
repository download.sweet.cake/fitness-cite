package com.filippov.fitness.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ContactsModel {
    private int id;
    private String phoneNumber;
    private String address;
    private String workingHours;
    private String vkLink;

}
