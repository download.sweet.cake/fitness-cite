package com.filippov.fitness.data;

import com.filippov.fitness.models.ServiceModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
public class JdbcServiceRepository implements ServiceRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcServiceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<ServiceModel> allListService() {
        log.info("Получение всех услуг из базы данных");
        return jdbcTemplate.query(
                "SELECT service_id, service_name, service_description, link_exists, image_link FROM service",
                this::mapRowToService);
    }

    @Override
    public Optional<ServiceModel> findById(Long id) {
        log.info("Поиск услуги по ID: {}", id);
        List<ServiceModel> results = jdbcTemplate.query(
                "SELECT service_id, service_name, service_description, link_exists, image_link FROM service WHERE service_id = ?",
                this::mapRowToService,
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public Optional<ServiceModel> findByName(String serviceName) {
        log.info("Поиск услуги по имени: {}", serviceName);
        List<ServiceModel> results = jdbcTemplate.query(
                "SELECT service_id, service_name, service_description, link_exists, image_link FROM service WHERE service_name = ?",
                this::mapRowToService,
                serviceName);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public ServiceModel save(ServiceModel serviceModel) {
        log.info("Сохранение новой услуги: {}", serviceModel);
        jdbcTemplate.update(
                "INSERT INTO service (service_name, service_description, link_exists, image_link) VALUES (?, ?, ?, ?)",
                serviceModel.getServiceName(),
                serviceModel.getServiceDescription(),
                serviceModel.getLinkExists(),
                serviceModel.getImageLink());
        return serviceModel;
    }

    @Override
    public ServiceModel update(ServiceModel serviceModel) {
        log.info("Обновление услуги: {}", serviceModel);
        jdbcTemplate.update(
                "UPDATE service SET service_name = ?, service_description = ?, link_exists = ?, image_link = ? WHERE service_id = ?",
                serviceModel.getServiceName(),
                serviceModel.getServiceDescription(),
                serviceModel.getLinkExists(),
                serviceModel.getImageLink(),
                serviceModel.getId());
        return serviceModel;
    }

    @Override
    public ServiceModel delete(ServiceModel serviceModel) {
        log.info("Удаление услуги: {}", serviceModel);
        jdbcTemplate.update(
                "DELETE FROM service WHERE service_id = ?",
                serviceModel.getId());
        return serviceModel;
    }

    private ServiceModel mapRowToService(ResultSet row, int rowNum) throws SQLException {
        return new ServiceModel(
                row.getLong("service_id"),
                row.getString("service_name"),
                row.getString("service_description"),
                row.getString("link_exists"),
                row.getString("image_link"));
    }
}
