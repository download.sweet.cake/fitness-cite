package com.filippov.fitness.data;

import com.filippov.fitness.models.ContactsModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
public class JdbcContactsRepository implements ContactsRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcContactsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public ContactsModel save(ContactsModel contacts) {
        log.info("Сохранение контактной информации: {}", contacts);
        jdbcTemplate.update(
                "INSERT INTO contacts (phone_number, address, working_hours, vk_link) VALUES (?, ?, ?, ?)",
                contacts.getPhoneNumber(),
                contacts.getAddress(),
                contacts.getWorkingHours(),
                contacts.getVkLink()
        );
        return contacts;
    }

    @Override
    public Optional<ContactsModel> findById(Long id) {
        log.info("Поиск контактной информации по ID: {}", id);
        List<ContactsModel> results = jdbcTemplate.query(
                "SELECT * FROM contacts WHERE id = ?",
                (rs, rowNum) -> new ContactsModel(
                        rs.getInt("id"),
                        rs.getString("phone_number"),
                        rs.getString("address"),
                        rs.getString("working_hours"),
                        rs.getString("vk_link")
                ),
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public Iterable<ContactsModel> listContacts() {
        log.info("Получение всех контактов из базы данных");
        return jdbcTemplate.query(
                "SELECT id, phone_number, address, working_hours, vk_link FROM contacts",
                this::mapRowToContacts);
    }

    @Override
    public void deleteById(Long id) {
        log.info("Удаление контактной информации с ID: {}", id);
        jdbcTemplate.update("DELETE FROM contacts WHERE id = ?", id);
    }

    private ContactsModel mapRowToContacts(ResultSet row, int rowNum) throws SQLException {
        return new ContactsModel(
                row.getInt("id"),
                row.getString("phone_number"),
                row.getString("address"),
                row.getString("working_hours"),
                row.getString("vk_link")
        );
    }
}
