package com.filippov.fitness.data;

import com.filippov.fitness.models.ServiceModel;

import java.util.Optional;

public interface ServiceRepository {
        Iterable<ServiceModel> allListService();
        Optional<ServiceModel> findById(Long id);
        Optional<ServiceModel> findByName(String serviceName);
        ServiceModel save(ServiceModel serviceModel);
        ServiceModel delete(ServiceModel serviceModel);
        ServiceModel update(ServiceModel serviceModel);
}
