package com.filippov.fitness.data;

import com.filippov.fitness.models.AdminModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
public class JdbcAdminRepository implements AdminRepository {

    private final JdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public JdbcAdminRepository(JdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Iterable<AdminModel> listAdmins() {
        log.info("Получение списка администраторов из базы данных");
        return jdbcTemplate.query(
                "SELECT id, username, password FROM admin",
                this::mapRowToAdmin);
    }

    @Override
    public Optional<AdminModel> findById(Long id) {
        log.info("Поиск администратора по ID: {}", id);
        List<AdminModel> results = jdbcTemplate.query(
                "SELECT id, username, password FROM admin WHERE id = ?",
                this::mapRowToAdmin,
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public Optional<AdminModel> findByUsername(String username) {
        log.info("Поиск администратора по имени: {}", username);
        List<AdminModel> results = jdbcTemplate.query(
                "SELECT id, username, password FROM admin WHERE username = ?",
                this::mapRowToAdmin,
                username);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public AdminModel save(AdminModel adminModel) {
        log.info("Сохранение нового администратора: {}", adminModel);
        String hashedPassword = passwordEncoder.encode(adminModel.getPassword());
        jdbcTemplate.update(
                "INSERT INTO admin (username, password) VALUES (?, ?)",
                adminModel.getUsername(),
                hashedPassword
        );
        return adminModel;
    }


    private AdminModel mapRowToAdmin(ResultSet row, int rowNum) throws SQLException {
        return new AdminModel(
                row.getLong("id"),
                row.getString("username"),
                row.getString("password")
        );
    }
}
