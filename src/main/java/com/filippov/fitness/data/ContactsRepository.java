package com.filippov.fitness.data;

import com.filippov.fitness.models.ContactsModel;

import java.util.Optional;

public interface ContactsRepository {
    ContactsModel save(ContactsModel contacts);
    Optional<ContactsModel> findById(Long id);
    Iterable<ContactsModel> listContacts();
    void deleteById(Long id);
}
