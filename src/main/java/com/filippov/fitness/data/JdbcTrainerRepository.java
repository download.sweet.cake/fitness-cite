package com.filippov.fitness.data;

import com.filippov.fitness.models.TrainerModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
public class JdbcTrainerRepository implements TrainerRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcTrainerRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<TrainerModel> listTrainers() {
        log.info("Получение списка тренеров из базы данных");
        return jdbcTemplate.query(
                "SELECT id, trainer_name, trainer_description, trainer_phone, trainer_vk_link FROM trainer",
                this::mapRowToTrainer);
    }
    @Override
    public void delete(Integer id) {
        log.info("Удаление тренера с ID: {}", id);
        String sql = "DELETE FROM trainer WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public Optional<TrainerModel> findById(Integer id) {
        log.info("Поиск тренера по ID: {}", id);
        List<TrainerModel> results = jdbcTemplate.query(
                "SELECT id, trainer_name, trainer_description, trainer_phone, trainer_vk_link FROM trainer WHERE id = ?",
                this::mapRowToTrainer,
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public TrainerModel save(TrainerModel trainerModel) {
        log.info("Сохранение нового тренера: {}", trainerModel);
        jdbcTemplate.update(
                "INSERT INTO trainer (trainer_name, trainer_description, trainer_phone, trainer_vk_link) VALUES (?, ?, ?, ?)",
                trainerModel.getTrainerName(),
                trainerModel.getTrainerDescription(),
                trainerModel.getTrainerPhone(),
                trainerModel.getTrainerVkLink()
        );
        return trainerModel;
    }

    private TrainerModel mapRowToTrainer(ResultSet row, int rowNum) throws SQLException {
        return new TrainerModel(
                row.getInt("id"),
                row.getString("trainer_name"),
                row.getString("trainer_description"),
                row.getString("trainer_phone"),
                row.getString("trainer_vk_link")
        );
    }
}
