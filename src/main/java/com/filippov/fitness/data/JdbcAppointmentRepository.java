package com.filippov.fitness.data;

import com.filippov.fitness.models.AppointmentModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
@Slf4j
@Repository
public class JdbcAppointmentRepository implements AppointmentRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcAppointmentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Iterable<AppointmentModel> allListAppointments() {
        log.info("Получение записей клиентов из базы данных, отсортированных по дате");
        return jdbcTemplate.query(
                "SELECT appointment_id, client_name, client_phone, appointment_date, trainer_name " +
                        "FROM appointment " +
                        "ORDER BY appointment_date DESC",
                this::mapRowToAppointment);
    }


    @Override
    public Optional<AppointmentModel> findById(Integer id) {
        log.info("Поиск записи клиента по ID: {}", id);
        List<AppointmentModel> results = jdbcTemplate.query(
                "SELECT appointment_id, client_name, client_phone, appointment_date, trainer_name FROM appointment WHERE appointment_id = ?",
                this::mapRowToAppointment,
                id);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public void save(AppointmentModel appointmentModel) {
        log.info("Сохранение записи клиента: {}", appointmentModel);
        jdbcTemplate.update(
                "INSERT INTO appointment (client_name, client_phone, appointment_date, trainer_name) " +
                        "VALUES (?, ?, ?, ?)",
                appointmentModel.getClientName(),
                appointmentModel.getClientPhone(),
                appointmentModel.getAppointmentDate(),
                appointmentModel.getTrainerName());
    }

    public void delete(Integer id) {
        log.info("Удаление записи клиента с ID: {}", id);
        String sql = "DELETE FROM appointment WHERE appointment_id = ?";
        jdbcTemplate.update(sql, id);
    }

    private AppointmentModel mapRowToAppointment(ResultSet row, int rowNum) throws SQLException {
        return new AppointmentModel(
                row.getInt("appointment_id"),
                row.getString("client_name"),
                row.getString("client_phone"),
                row.getDate("appointment_date"),
                row.getString("trainer_name"));
    }
}
