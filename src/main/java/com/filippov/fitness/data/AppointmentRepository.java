package com.filippov.fitness.data;

import java.util.Optional;
import com.filippov.fitness.models.AppointmentModel;

public interface AppointmentRepository {
    Iterable<AppointmentModel> allListAppointments();
    Optional<AppointmentModel> findById(Integer id);
    void save(AppointmentModel appointmentModel);
    void delete(Integer id);

}
