package com.filippov.fitness.data;

import com.filippov.fitness.models.AdminModel;

import java.util.Optional;

public interface AdminRepository {
    Iterable<AdminModel> listAdmins();
    Optional<AdminModel> findById(Long id);
    Optional<AdminModel> findByUsername(String username);
    AdminModel save(AdminModel adminModel);
}
