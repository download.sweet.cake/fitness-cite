package com.filippov.fitness.data;

import com.filippov.fitness.models.TrainerModel;

import java.util.Optional;

public interface TrainerRepository {
    Iterable<TrainerModel> listTrainers();

    void delete(Integer id);

    Optional<TrainerModel> findById(Integer id);
    TrainerModel save(TrainerModel trainerModel);
}
