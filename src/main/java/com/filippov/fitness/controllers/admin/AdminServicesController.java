package com.filippov.fitness.controllers.admin;

import com.filippov.fitness.data.ServiceRepository;
import com.filippov.fitness.models.AdminModel;
import com.filippov.fitness.models.ServiceModel;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/admin")
@Controller
public class AdminServicesController {

    @Autowired
    private ServiceRepository serviceRepository;

    @ModelAttribute
    public void addServicesToModel(Model model) {
        model.addAttribute("services", serviceRepository.allListService());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/services")
    public String services(Model model) {
        log.info("Доступ к странице услуг (админ)");
        model.addAttribute("currentPage", "adminservices");
        return "admin/services";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/services/add")
    public String changeService(Model model) {
        log.info("Доступ к странице добавления услуги");
        ServiceModel serviceModel = new ServiceModel();
        model.addAttribute("serviceModel", serviceModel);
        return "admin/addChangeService";
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/services/add")
    public String addService(Model model, @Valid @ModelAttribute ServiceModel serviceModel, Errors errors) {
        log.info("Добавление новой услуги: {}", serviceModel.getServiceName());
        if (!errors.hasErrors()) {
            serviceRepository.save(serviceModel);
            return "redirect:/admin/services";
        }
        model.addAttribute("serviceModel", serviceModel);
        model.addAttribute("errors", errors);
        return "admin/addChangeService";
    }
}
