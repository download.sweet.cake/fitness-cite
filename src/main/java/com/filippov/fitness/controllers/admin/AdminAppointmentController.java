package com.filippov.fitness.controllers.admin;

import com.filippov.fitness.data.AppointmentRepository;
import com.filippov.fitness.models.AppointmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/admin")
public class AdminAppointmentController {

    private final AppointmentRepository appointmentRepository;

    @Autowired
    public AdminAppointmentController(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/appointment")
    public String appointment(Model model) {
        log.info("Получение списка записей для администратора");
        model.addAttribute("appointments", appointmentRepository.allListAppointments());
        model.addAttribute("appointmentModel", new AppointmentModel());
        return "admin/appointment";
    }
    @PostMapping("/appointment/delete/{id}")
    public String deleteAppointment(@PathVariable Integer id) {
        log.info("Удаление записи клиента с ID: {}", id);
        appointmentRepository.delete(id);
        return "redirect:/admin/appointment"; // Перенаправляем обратно на страницу списка записей
    }
}
