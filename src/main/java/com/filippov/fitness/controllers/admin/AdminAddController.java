package com.filippov.fitness.controllers.admin;

import com.filippov.fitness.data.AdminRepository;
import com.filippov.fitness.models.AdminModel;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequestMapping("/admin")
@Controller
public class AdminAddController {

    @Autowired
    private AdminRepository adminRepository;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/changeAdmin")
    public String changeAdmin(Model model) {
        log.info("Доступ к странице изменения администратора");
        model.addAttribute("currentPage", "changeAdmin");
        AdminModel adminModel = new AdminModel();
        model.addAttribute("adminModel", adminModel);
        return "admin/changeAdmin";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/changeAdmin")
    public String updateAdmin(Model model, @Valid AdminModel adminModel, Errors errors) {
        log.info("Обновление данных администратора");
        if (!errors.hasErrors()) {
            adminRepository.save(adminModel);
            return "redirect:/admin/changeAdmin?success=true";
        }
        return "admin/changeAdmin";
    }
}
