package com.filippov.fitness.controllers.admin;

import com.filippov.fitness.data.TrainerRepository;
import com.filippov.fitness.models.AppointmentModel;
import com.filippov.fitness.models.ServiceModel;
import com.filippov.fitness.models.TrainerModel;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/admin")
@Controller
public class AdminTrainersController {

    @Autowired
    private TrainerRepository trainerRepository;

    @ModelAttribute
    public void addTrainersToModel(Model model) {
        model.addAttribute("trainers", trainerRepository.listTrainers());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/team")
    public String team(Model model) {
        log.info("Доступ к странице тренеров (админ)");
        model.addAttribute("currentPage", "adminteam");
        return "admin/team";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/team/add")
    public String addTrainer(Model model) {
        log.info("Доступ к странице добавления тренера");
        TrainerModel trainerModel = new TrainerModel();
        model.addAttribute("trainerModel", trainerModel);
        return "admin/addChangeTrainer";
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/team/add")
    public String addService(Model model, @Valid @ModelAttribute TrainerModel trainerModel, Errors errors) {
        log.info("Добавление нового тренера: {}", trainerModel.getTrainerName());
        if (!errors.hasErrors()) {
            trainerRepository.save(trainerModel);
            return "redirect:/admin/team";
        }
        model.addAttribute("trainerModel", trainerModel);
        model.addAttribute("errors", errors);
        return "admin/addChangeTeam";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/team/delete/{id}")
    public String deleteTrainer(@PathVariable Integer id) {
        log.info("Удаление тренера с ID: {}", id);
        trainerRepository.delete(id);
        return "redirect:/admin/team";
    }
}
