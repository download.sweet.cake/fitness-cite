package com.filippov.fitness.controllers.admin;

import com.filippov.fitness.data.ContactsRepository;
import com.filippov.fitness.models.ContactsModel;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequestMapping("/admin")
@Controller
public class AdminContactsController {

    @Autowired
    private ContactsRepository contactsRepository;

    @ModelAttribute
    public void addContactsToModel(Model model) {
        log.info("Добавление контактов в модель");
        model.addAttribute("contacts", contactsRepository.listContacts());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/contacts")
    public String contacts(Model model) {
        log.info("Доступ к странице с контактами (админ)");
        model.addAttribute("currentPage", "admincontacts");
        ContactsModel contactsModel = new ContactsModel();
        model.addAttribute("contactsModel", contactsModel);
        return "admin/contacts";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/contacts")
    public String updateContacts(Model model, @Valid ContactsModel contactsModel, Errors errors) {
        log.info("Обновление контактной информации");
        if (!errors.hasErrors()) {
            contactsRepository.save(contactsModel);
            return "redirect:/admin/contacts?success=true";
        }
        return "admin/contacts";
    }
}
