package com.filippov.fitness.controllers.user;

import com.filippov.fitness.data.ContactsRepository;
import com.filippov.fitness.data.ServiceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Slf4j
@Controller
@RequiredArgsConstructor
public class UserServiceController {
    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ContactsRepository contactsRepository;

    @ModelAttribute
    public void addContactsToModel(Model model) {
        log.info("Добавление контактов в модель");
        model.addAttribute("contacts", contactsRepository.listContacts());
    }

    @ModelAttribute
    public void addServicesToModel(Model model) {
        log.info("Добавление услуг в модель");
        model.addAttribute("services", serviceRepository.allListService());
    }

    @GetMapping("/services")
    public String services(Model model) {
        log.info("Доступ к странице услуг");
        model.addAttribute("currentPage", "services");
        return "user/services";
    }

    @GetMapping("/services/gym")
    public String showGym(Model model) {
        log.info("Доступ к странице тренажерного зала");
        model.addAttribute("currentPage", "gym");
        return "user/gym";
    }

    @GetMapping("/services/yoga")
    public String showYoga(Model model) {
        log.info("Доступ к странице йоги");
        model.addAttribute("currentPage", "yoga");
        return "user/yoga";
    }

    @GetMapping("/services/martial-arts")
    public String showMartial(Model model) {
        log.info("Доступ к странице боевых искусств");
        model.addAttribute("currentPage", "martial-arts");
        return "user/martial-arts";
    }

}
