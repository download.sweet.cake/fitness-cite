package com.filippov.fitness.controllers.user;

import com.filippov.fitness.data.AppointmentRepository;
import com.filippov.fitness.data.TrainerRepository;
import com.filippov.fitness.models.AppointmentModel;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Slf4j
@Controller
public class AppointmentController {

    @Autowired
    private TrainerRepository trainerRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @ModelAttribute
    public void addTrainersToModel(Model model) {
        log.info("Добавление тренеров в модель");
        model.addAttribute("trainers", trainerRepository.listTrainers());
    }

    @GetMapping("/team/appointment")
    public String appointment(@RequestParam(value = "trainerName", required = false) String trainerName,
                              @RequestParam(value = "success", required = false) String success, Model model) {
        log.info("Доступ к странице записи");
        model.addAttribute("appointments", appointmentRepository.allListAppointments());
        AppointmentModel appointmentModel = new AppointmentModel();
        appointmentModel.setTrainerName(trainerName);
        model.addAttribute("success", success != null);
        model.addAttribute("appointmentModel", appointmentModel);
        return "user/appointment";
    }

    @PostMapping("/team/appointment")
    public String createAppointment(Model model, @Valid AppointmentModel appointmentModel, Errors errors, @RequestParam(value = "consent", required = false) String consent) {
        log.info("Добавление запись клиента к тренеру");
        if (!errors.hasErrors() && "on".equals(consent)) {
            appointmentModel.setAppointmentDate(new Date());
            appointmentRepository.save(appointmentModel);
            return "redirect:/team/appointment?success=true";
        }
        return "/user/appointment";
    }
}
