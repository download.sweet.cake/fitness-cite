package com.filippov.fitness.controllers.user;

import com.filippov.fitness.data.ContactsRepository;
import com.filippov.fitness.data.TrainerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Slf4j
@Controller
@RequiredArgsConstructor
public class UserMainPageController {
    @Autowired
    private ContactsRepository contactsRepository;

    @ModelAttribute
    public void addContactsToModel(Model model) {
        log.info("Добавление контактов в модель");
        model.addAttribute("contacts", contactsRepository.listContacts());
    }
    @GetMapping("/")
    public String club(Model model) {
        log.info("Доступ к главной странице (клуб)");
        model.addAttribute("currentPage", "club");
        return "user/mainpage";
    }
    @GetMapping("/sitemap.xml")
    public String sitemap() {
        return "user/sitemap.xml";
    }

    @GetMapping("/prices")
    public String prices(Model model) {
        log.info("Доступ к странице с ценами");
        model.addAttribute("currentPage", "prices");
        return "user/prices";
    }

    @GetMapping("/contacts")
    public String contacts(Model model) {
        log.info("Доступ к странице контактов");
        model.addAttribute("currentPage", "contacts");
        return "user/contacts";
    }
}
