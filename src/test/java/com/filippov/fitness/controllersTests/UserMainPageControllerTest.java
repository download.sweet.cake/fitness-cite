//package com.filippov.fitness.controllersTests;
//
//import com.filippov.fitness.controllers.user.UserMainPageController;
//import com.filippov.fitness.data.ContactsRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.ui.Model;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.*;
//
//class UserMainPageControllerTest {
//
//    @Mock
//    Model model;
//
//    @Mock
//    ContactsRepository contactsRepository;
//
//    UserMainPageController controller;
//
//
//    @Test
//    void club() {
//        String viewName = controller.club(model);
//        assertEquals("user/mainpage", viewName);
//        verify(model, times(1)).addAttribute("currentPage", "club");
//    }
//
//    @Test
//    void sitemap() {
//        String viewName = controller.sitemap();
//        assertEquals("user/sitemap.xml", viewName);
//    }
//
//    @Test
//    void prices() {
//        String viewName = controller.prices(model);
//        assertEquals("user/prices", viewName);
//        verify(model, times(1)).addAttribute("currentPage", "prices");
//    }
//
//    @Test
//    void contacts() {
//        // Act
//        String viewName = controller.contacts(model);
//
//        // Assert
//        assertEquals("user/contacts", viewName);
//        verify(model, times(1)).addAttribute("currentPage", "contacts");
//    }
//}
