//package com.filippov.fitness.controllersTests;
//
//import com.filippov.fitness.controllers.user.UserServiceController;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.ui.Model;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.*;
//
//class UserServiceControllerTest {
//
//    @Mock
//    Model model;
//
//    @Test
//    void showServices() {
//        // Arrange
//        MockitoAnnotations.openMocks(this);
//        UserServiceController controller = new UserServiceController();
//
//        // Act
//        String viewName = controller.showServices(model);
//
//        // Assert
//        assertEquals("user/services", viewName);
//        verify(model, times(1)).addAttribute("activeGym", false);
//        verify(model, times(1)).addAttribute("activeYoga", false);
//        verify(model, times(1)).addAttribute("activeMartial", false);
//        verify(model, times(1)).addAttribute("activeGym", false);
//        verify(model, times(1)).addAttribute("activeClub", false);
//        verify(model, times(1)).addAttribute("activeContacts", false);
//        verify(model, times(1)).addAttribute("activePrices", false);
//        verify(model, times(1)).addAttribute("activeTeam", false);
//        verify(model, times(1)).addAttribute("activeServices", true);
//    }
//
//    // Add similar tests for showGym(), showYoga(), and showMartial() methods
//}
