//package com.filippov.fitness.JDBCTests;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.Mockito.when;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import com.filippov.fitness.data.JdbcTrainerRepository;
//import com.filippov.fitness.models.TrainerModel;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.jdbc.core.JdbcTemplate;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyString;
//import org.springframework.jdbc.core.RowMapper;
//
//
//public class JdbcTrainerRepositoryTests {
//
//    @Mock
//    private JdbcTemplate jdbcTemplate;
//
//    @InjectMocks
//    private JdbcTrainerRepository trainerRepository;
//
//    @BeforeEach
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//    }
//
//    @Test
//    public void testListTrainers() {
//        // Arrange
//        List<TrainerModel> trainers = new ArrayList<>();
//        trainers.add(new TrainerModel(1, "John Doe", "Experienced trainer", "123-456-7890", "https://vk.com/johndoe"));
//        trainers.add(new TrainerModel(2, "Jane Smith", "Certified yoga instructor", "987-654-3210", "https://vk.com/janesmith"));
//        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(trainers);
//
//        // Act
//        Iterable<TrainerModel> result = trainerRepository.listTrainers();
//
//        // Assert
//        assertTrue(result.iterator().hasNext());
//        assertEquals(trainers.size(), ((List<TrainerModel>) result).size());
//    }
//
//    @Test
//    public void testFindById() {
//        // Arrange
//        TrainerModel trainer = new TrainerModel(1, "John Doe", "Experienced trainer", "123-456-7890", "https://vk.com/johndoe");
//        when(jdbcTemplate.query(anyString(), any(RowMapper.class), anyLong())).thenReturn(List.of(trainer));
//
//        // Act
//        Optional<TrainerModel> result = trainerRepository.findById(1L);
//
//        // Assert
//        assertTrue(result.isPresent());
//        assertEquals(trainer, result.get());
//    }
//
//    @Test
//    public void testFindById_NotFound() {
//        // Arrange
//        when(jdbcTemplate.query(anyString(), any(RowMapper.class), anyLong())).thenReturn(List.of());
//
//        // Act
//        Optional<TrainerModel> result = trainerRepository.findById(1L);
//
//        // Assert
//        assertFalse(result.isPresent());
//    }
//
//    @Test
//    public void testSave() {
//        // Arrange
//        TrainerModel trainer = new TrainerModel(1, "John Doe", "Experienced trainer", "123-456-7890", "https://vk.com/johndoe");
//
//        // Act
//        TrainerModel result = trainerRepository.save(trainer);
//
//        // Assert
//        assertEquals(trainer, result);
//    }
//}
