//package com.filippov.fitness.modelsTests;
//
//import com.filippov.fitness.models.TrainerModel;
//import org.junit.jupiter.api.Test;
//import jakarta.validation.Validation;
//import jakarta.validation.Validator;
//import jakarta.validation.ConstraintViolation;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//public class ModelTests {
//
//    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
//
//
//    @Test
//    void testInvalidTrainerModel() {
//        TrainerModel trainer = new TrainerModel(); // Создание экземпляра без передачи аргументов
//        Set<ConstraintViolation<TrainerModel>> violations = validator.validate(trainer);
//        assertFalse(violations.isEmpty());
//    }
//
//    @Test
//    void testValidTrainerModel() {
//        TrainerModel trainer = new TrainerModel(1, "John Doe", "Experienced trainer", "123456789", "http://vk.com/johndoe");
//        Set<ConstraintViolation<TrainerModel>> violations = validator.validate(trainer);
//        assertTrue(violations.isEmpty());
//    }
//
//    @Test
//    void testEmptyTrainerName() {
//        TrainerModel trainer = new TrainerModel(1, "", "Experienced trainer", "123456789", "http://vk.com/johndoe");
//        Set<ConstraintViolation<TrainerModel>> violations = validator.validate(trainer);
//        assertFalse(violations.isEmpty());
//    }
//
//    @Test
//    void testEmptyTrainerDescription() {
//        TrainerModel trainer = new TrainerModel(1, "John Doe", "", "123456789", "http://vk.com/johndoe");
//        Set<ConstraintViolation<TrainerModel>> violations = validator.validate(trainer);
//        assertFalse(violations.isEmpty());
//    }
//
//    @Test
//    void testNullTrainerName() {
//        TrainerModel trainer = new TrainerModel(1, null, "Experienced trainer", "123456789", "http://vk.com/johndoe");
//        Set<ConstraintViolation<TrainerModel>> violations = validator.validate(trainer);
//        assertFalse(violations.isEmpty());
//    }
//
//    @Test
//    void testNullTrainerDescription() {
//        TrainerModel trainer = new TrainerModel(1, "John Doe", null, "123456789", "http://vk.com/johndoe");
//        Set<ConstraintViolation<TrainerModel>> violations = validator.validate(trainer);
//        assertFalse(violations.isEmpty());
//    }
//}
